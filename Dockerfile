ARG PARENT_IMAGE=csf-docker-delivered.repo.lab.pl.alcatel-lucent.com/java_base/17/nano
ARG PARENT_VERSION=17.0.5.0.8.rocky8.7-20221226

FROM ${PARENT_IMAGE}:${PARENT_VERSION}